require.config({
    baseUrl: "*add a base url for your modules here*",
    shim: {
        jquery: {
            exports: 'jQuery'
        }
    },

    paths: {
        jquery: [
            "//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min",
            "vendor/jquery"
        ]
    }
});

requirejs(['jquery', 'modules/animation', 'modules/log'], function ($, animation, log) {


    try
    {
        //setup cloud animation
        if($('*your holder*').length && !$('html').hasClass('lt-ie9'))
        {


            //the actual animation config, see modules/animation and the sass file _animationLib.sass

            animation.config({
                elements: '.cloud',
                area: '#clouds-holder',
                delay: 500,
                timeout: 0,
                transitions: ['ease'],
                speeds: ['slow'],
                multiply: 'all',
                distance: [30,260],
                loop: true
            });


        }

    }
    catch(e)
    {
        //safe ie logging, see modules/log.js
        log.log(e.message);
    }

});