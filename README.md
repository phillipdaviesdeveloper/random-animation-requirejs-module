# Hi!

Quick bits:

The main js organises and calls the required modules (jquery, log, animation), a config object is then passed to the new animation instance
to set up all the bits and bobs.

The animations themselves use the compass animation lib, so that will need to be installed as well:

*in the deploy.rb file add the requirement for the animation plugin and check that you have all the gems installed:

    gem list

    if you need to install the animate gem do so with  gem install animation --pre


I'll add a proper example and docs as soon as I get a chance :)


