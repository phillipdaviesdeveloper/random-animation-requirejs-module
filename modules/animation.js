define(['modules/log', 'modules/util'], function(log, util)
    {

        var that;

        //create the function to prototype
        function animationEngine()
        {

            that = this;

            //ensure animationEngine the module is initialised correctly
            if (!(this instanceof animationEngine)) {
                throw new TypeError("Animation Engine constructor cannot be called as a function.");
            }


            // properties

            /************************************
             *
             * Core animation properties, should only be set via the config object
             *
             ***********************************/

            //the area that contains the elements
            this.area = false;


            //elements that should be animated
            this.animElements = new Array();




            //the direction the elements will animate in
            this.directions = ['left', 'right'];

            //the speed of the animation (should link to css classes)
            this.animTiming = ['slow', 'medium', 'fast'];


            //the speed of the animation (should link to css classes)
            this.animTransition = ['ease', 'linear', 'bezier'];

            // a min / max integer to range the random animation distance
            this.animDistance = [0, 850];




            // delay between animations, in milliseconds
            this.animTimeout = 1000;

            //delay between multiple element animation
            this.animDelay = 500;

            //Number of times to fire the animations
            // true = infinate
            // int = specific number of loops
            this.loops = 1

            // number of elemets to animate simultaneously
            this.multiply = 1;

            // boundry for the animation
            this.areaRange = new Array();
            this.areaRangeOffset = new Array();


            //in animation flags
            this.animCount = 1;
            this.animAnimating = false;

            //Animation array to contain individual animation settings
            this.anim = [];

            //Array to hold all the items currently animating
            this.animCurrent = [];

            //current animation frame
            this.animFrame = false;

        }





        // "Private" methods

        /***********************************
         *
         *  These methods are used by the module specifically and have no external global use
         *  or indeed any use by the client code.
         *
         *  They have been defined as 'static' methods so closure will only allow them to be accessed in this file.
         *
         **********************************/


        /***********************************
         *
         * Finds the amount of unique elements to animate
         *
         * Sends each selected element to be configured for animation and once all
         * items are configured calls the fireAnimation method to start the animation.
         *
         **********************************/
         function queAnimation()
        {

            //Checking if the animation is complete / ready to go
            if(!that.animAnimating)
            {

                //find the items to animate
                var amount = (that.multiply === 'all') ? that.animElements.length : that.multiply;
                var items = util.randomUniqueArrayItems(that.animElements, amount);

                //send the items to be processed for animation
                var i=0;

                while(i<items.length)
                {
                    processAnimation(items[i]);
                    ++i;
                }

                var i = 0;

                while(i<that.anim.length)
                {

                    //start the animation, with delay between multiple animations
                    delayAnimation(i);

                    ++i;
                }


                that.animAnimating = true;
                requestAnimationFrame(checkAnimation);

            }

        }


        /***********************************
         *
         * Delays the individual animations
         *
         **********************************/
        function delayAnimation(item)
        {

            var delay = (that.animDelay * item)

            setTimeout(function(){
                fireAnimation(that.anim[item]);
            }, delay);
        }


        /***********************************
         *
         * Sets the individual item animations
         *
         * Accepts a single element and generates an anim Object with randomised animation configuration.
         * The anim Object is then pushed into the anim property of the animationEngine Object.
         *
         **********************************/
        function processAnimation(item)
        {

            var direction = util.randomUniqueArrayItems(that.directions, 1);
            var speed = util.randomUniqueArrayItems(that.animTiming, 1);
            var transition = util.randomUniqueArrayItems(that.animTransition, 1);

            //do all the css class assignment, checking for completion, assigning of flags for completion
            var anim = {
                id: item,
                distance: util.getRandomInt(that.animDistance[0], that.animDistance[1]),
                direction: direction[0],
                transition: transition[0],
                speed: speed[0],
                finish: false,
                complete: false
            }

            that.anim.push(anim);
        }


        /***********************************
         *
         * Applies the animation to the elements
         *
         * Accepts a single element and generates an anim Object with randomised animation configuration.
         * The anim Object is then pushed into the anim property of the animationEngine Object.
         *
         **********************************/
        function fireAnimation(item)
        {

            var cur = item,
                curId = $(cur.id),
                curDist = 0,
                curPosH = curId.position().left,
                curPosV = curId.position().top;

            //find the direction and normalise the distance / direction css property
            switch(cur.direction)
            {
                case 'left':
                    curDist = constrainAnimation(cur.distance,cur.direction, curPosH);
                    cur.finish = curDist;
                    curId.addClass('animate-left');
                    curId.css('left', curDist);
                break;

                case 'right':
                    curDist = constrainAnimation(cur.distance,cur.direction, curPosH);
                    cur.finish = curDist;
                    curId.addClass('animate-left');
                    curId.css('left', curDist);
                break;

                case 'up':
                    curDist = constrainAnimation(cur.distance,cur.direction, curPosV);
                    cur.finish = curDist;
                    curId.addClass('animate-top');
                    curId.css('top', curDist);
                break;

                case 'down':
                    curDist = constrainAnimation(cur.distance,cur.direction, curPosV);
                    cur.finish = curDist;
                    curId.addClass('animate-top');
                    curId.css('top', curDist);
                break;

                default:
                    throw new Error('Sorry, that direction is not recognised, please check your config method and only use that available directions i.e. ["left", "right", "up", "down"]');
                break;

            }

            //set speed and transition
            curId.addClass('animate-'+cur.speed);
            curId.addClass('animate-'+cur.transition);

            that.animCurrent.push(item);

        }


        /***********************************
         *
         * Checks the state of the animation and loops / resets appropriately
         *
         **********************************/
        function checkAnimation()
        {
//          log.log(that.anim.length == that.animCurrent.length);
//          log.log('animating = '+that.animAnimating)


            //are all the elements animating?
            if(that.anim.length == that.animCurrent.length)
            {

                //Checking if the animation is complete / ready to go
                var i = 0;

                while(i<that.anim.length)
                {
                    if(that.anim[i].direction = 'left')
                    {
                        if(that.anim[i].finish == $(that.anim[i].id).position().left)
                        {
                            that.anim[i].complete = true;
                        }
                    }
                    else
                    {
                        if(that.anim[i].finish == $(that.anim[i].id).position().top)
                        {
                            that.anim[i].complete = true;
                        }
                    }

                    ++i;
                }

                that.animAnimating = false;

                var i = 0;

                while(i<that.anim.length)
                {

                    if(that.anim[i].complete === false)
                    {
                        that.animAnimating = true;
                    }

                    ++i;
                }


            }


            //check the animation state and either call the next frame, loop or complete the animation
            if(that.animAnimating)
            {
                this.animFrame = requestAnimationFrame(checkAnimation);
            }
            else if(that.animCount <= that.loops || that.loops === true)
            {
                setTimeout(function(){
                    cancelAnimationFrame(this.animFrame);
                    resetAnimation();
                    queAnimation();
                    that.animCount++;
                }, that.animTimeout);
            }
            else
            {
                resetAnimation();
            }


        }


        /***********************************
         *
         * Restrains the animation to the container
         * so items do not stray too far off the set area + the offset
         *
         **********************************/
        function constrainAnimation(distance, direction, currentPosition)
        {
            //find the width and height of the animation container
            that.areaRange = [that.area.width(), that.area.height()];

            var newPosition = currentPosition;
            var horizLimit = that.areaRange[0];
            var vertLimit = that.areaRange[1];

            switch(direction)
            {
                case 'left':

                    newPosition = (currentPosition-distance);

                    if (0 < that.areaRangeOffset.length)
                    {
                        if(newPosition < (0-that.areaRangeOffset[0]))
                            newPosition = (0-that.areaRangeOffset[0])
                    }
                    else if(newPosition < 0)
                    {
                        newPosition = 0;
                    }


                break;

                case 'right':

                    newPosition = (currentPosition+distance);

                    if (0 < that.areaRangeOffset.length)
                    {
                        if(newPosition > (horizLimit+that.areaRangeOffset[0]))
                            newPosition = (horizLimit+that.areaRangeOffset[0])
                    }
                    else if(newPosition > horizLimit)
                    {
                        newPosition = horizLimit;
                    }

                break;

                case 'up':

                    newPosition = (currentPosition-distance);

                    if (0 < that.areaRangeOffset.length)
                    {
                        if(newPosition < (0-that.areaRangeOffset[1]))
                            newPosition = (0-that.areaRangeOffset[1])
                    }
                    else if(newPosition < 0)
                    {
                        newPosition = 0;
                    }

                break;

                case 'down':

                    newPosition = (currentPosition+distance);

                    if (0 < that.areaRangeOffset.length)
                    {
                        if(newPosition > (vertLimit+that.areaRangeOffset[1]))
                            newPosition = (vertLimit+that.areaRangeOffset[1])
                    }
                    else if(newPosition > vertLimit)
                    {
                        newPosition = vertLimit;
                    }

                break;
            }

            return newPosition;

        }


        /***********************************
         *
         * Clears classes and tracking arrays
         *
         **********************************/
        function resetAnimation()
        {

            var i = 0;

            while(i<that.anim.length)
            {

                $(that.anim[i].id).attr('class',"cloud");

                ++i;
            }

            that.anim = [];
            that.animCurrent = [];
        }


        // methods

        /**********************
        *
        * These methods are essentially the public getter, setter type to
        * be used by the client coder
        *
        **********************/

        animationEngine.prototype = {


            constructor: animationEngine,



                /*************************************
                *
                * The configuration method, accepts a js object containing
                * the client coder configuration for the animation object instance
                *
                *************************************/
                config:  function(conf)
                {

                        //checking config is valid
                        if(conf instanceof Object)
                        {

                            this.area = $(conf.area);

                            // find the elements to animate
                            var elements = this.area.find(conf.elements);


                            //check the elements passed exist
                            if(elements.length === 0)
                                throw new Error('No elements found to animate');


                            // assign found elements to the property
                            for(var x=0; x < elements.length; x++)
                                this.animElements[x] = elements[x];


                            //if a directions argument has been passed
                            if(conf.directions !== undefined)
                            {
                                if(conf.directions instanceof Array)
                                {
                                    //store the directions configuration
                                    this.directions = conf.directions;
                                }
                                else
                                {
                                    throw new Error('Directions argument should be an array e.g. ["left", "right"]');
                                }
                            }


                            //if a timings argument has been passed
                            if(conf.speeds !== undefined)
                            {
                                if(conf.speeds instanceof Array)
                                {
                                    //store the timing configuration
                                    this.animTiming = conf.speeds;
                                }
                                else
                                {
                                    throw new Error('Speeds argument should be an array e.g. ["fast", "slow"]');
                                }
                            }


                            //if a transitions argument has been passed
                            if(conf.transitions !== undefined)
                            {
                                if(conf.transitions instanceof Array)
                                {
                                    //store the timing configuration
                                    this.animTransition = conf.transitions;
                                }
                                else
                                {
                                    throw new Error('Transition argument should be an array e.g. ["bezier", "ease"]');
                                }
                            }


                            //if a distance argument has been passed
                            if(conf.distance !== undefined)
                            {
                                if(conf.distance instanceof Array)
                                {
                                    //store the timing configuration
                                    this.animDistance = conf.distance;
                                }
                                else
                                {
                                    throw new Error('Distance argument should be an array with two properties containing integers e.g. [0,500]');
                                }
                            }


                            //if a delay argument has been passed
                            if(conf.delay !== undefined)
                            {
                                //is the passed delay property a number?
                                if((typeof conf.delay === "number") && Math.floor(conf.delay) === conf.delay)
                                {
                                    //store the timing configuration
                                    this.animDelay = conf.delay ;
                                }
                                else
                                {
                                    throw new Error('Delay argument should be an integer representing milliseconds e.g. 1000');
                                }
                            }


                            //if a delay argument has been passed
                            if(conf.timeout !== undefined)
                            {
                                //is the passed delay property a number?
                                if((typeof conf.timeout === "number") && Math.floor(conf.timeout) === conf.timeout)
                                {
                                    //store the timing configuration
                                    this.animTimeout = conf.timeout;
                                }
                                else
                                {
                                    throw new Error('Timeout argument should be an integer representing milliseconds e.g. 1000');
                                }
                            }


                            //if a loop argument has been passed
                            if(conf.loop !== undefined)
                            {
                                //is the loop property is a number or false
                                if((typeof conf.loop === "number") && Math.floor(conf.loop) === conf.loop || conf.loop === true)
                                {
                                    //store the loop configuration
                                    this.loops = conf.loop ;
                                }
                                else
                                {
                                    throw new Error('Loop argument should be an integer representing amount of loops or true (for infinate loops)');
                                }
                            }


                            //if a multiply argument has been passed
                            if(conf.multiply !== undefined)
                            {
                                //is the multiply property is a number
                                if((typeof conf.multiply === "number") && Math.floor(conf.multiply) === conf.multiply || conf.multiply === "all")
                                {
                                    //store the multiply configuration
                                    this.multiply = conf.multiply ;
                                }
                                else
                                {
                                    throw new Error('Multiply argument should be an integer representing amount of items to animate at the same time');
                                }
                            }


                            //if a offset argument has been passed
                            if(conf.areaoffest !== undefined)
                            {
                                //is the offset is an array
                                if(conf.areaoffest instanceof Array)
                                {
                                    //store the timing configuration
                                    this.areaRangeOffset = conf.areaoffest;
                                }
                                else
                                {
                                    throw new Error('Areaoffset argument should be an array with 4 properties as integers (top, left, bottom, right) e.g. [10,10,10,50]');
                                }
                            }

                        }
                        else
                        {
                            throw new Error('Passed configuration should be an Object e.g. {elements: ".test"}');
                        }

                    this.init();

                },

                /***********************
                *
                * Initialises the animation using the provided configuration details
                *
                ************************/

                init: function()
                {

                    //set up the global request frame object for browser compatibility
                    util.requestFrame();

                    // see private methods
                    queAnimation();

                }

        }



        // RequireJS return


        /***********************
        *
        * Usually the methods would be returned here, but as we are using a
        * object and a protype we simply pass the reference to the constructor / create a new instance
        *
        ***********************/
        return new animationEngine;

    }
);